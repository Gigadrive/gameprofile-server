package com.gigadrivegroup.gameprofileserver

import com.gigadrivegroup.gameprofileserver.handler.HelloWorldHandler
import com.gigadrivegroup.gameprofileserver.handler.ProfileRequestHandler
import com.gigadrivegroup.kotlincommons.feature.CommonsManager
import com.gigadrivegroup.kotlincommons.feature.bind
import com.gigadrivegroup.kotlincommons.feature.initDependencyInjection
import com.sun.net.httpserver.HttpServer
import java.net.InetSocketAddress

public class Bootstrap {
    public companion object {
        @JvmStatic
        public fun main(args: Array<String>) {
            val port: Int = (System.getProperty("PORT") ?: "8080").toInt()
            println("Starting web server on port $port...")

            initDependencyInjection()
            bind(MojangProfileManager())

            val server = HttpServer.create(InetSocketAddress(port), 0)
            server.executor = null

            // Register handlers
            ProfileRequestHandler(server).bind()
            HelloWorldHandler(server).bind()

            server.start()
            bind(server)

            println("Server is running.")

            Runtime.getRuntime()
                .addShutdownHook(
                    object : Thread() {
                        override fun run() {
                            println("Stopping server...")
                            server.stop(3)

                            println("Server stopped.")

                            println("Shutting down managers...")

                            CommonsManager.shutdown()

                            println("Shut down managers.")

                            println("Goodbye.")

                            sleep(500)
                        }
                    })
        }
    }
}
