package com.gigadrivegroup.gameprofileserver.handler

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer

public class HelloWorldHandler(server: HttpServer) : Handler(server, "/") {
    override fun handleExchange(exchange: HttpExchange): Unit =
        writeResponse(exchange, "\"Hello world!\"", 200)
}
