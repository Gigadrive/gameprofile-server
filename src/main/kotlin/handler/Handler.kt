package com.gigadrivegroup.gameprofileserver.handler

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import com.sun.net.httpserver.HttpServer
import java.util.HashMap

public abstract class Handler(private val server: HttpServer, private val route: String) :
    HttpHandler {
    public fun bind() {
        server.createContext(route, this)
    }

    public abstract fun handleExchange(exchange: HttpExchange)

    override fun handle(exchange: HttpExchange?) {
        exchange?.let { handleExchange(it) }
    }

    protected fun queryToMap(query: String?): Map<String, String> {
        if (query == null) {
            return mapOf()
        }

        val result: MutableMap<String, String> = HashMap()
        for (param in query.split("&").toTypedArray()) {
            val entry = param.split("=").toTypedArray()
            if (entry.size > 1) {
                result[entry[0]] = entry[1]
            } else {
                result[entry[0]] = ""
            }
        }
        return result
    }

    protected fun writeResponse(exchange: HttpExchange, response: String, httpCode: Int) {
        exchange.responseHeaders.set("Content-Type", "application/json; charset=UTF-8")
        exchange.sendResponseHeaders(httpCode, response.length.toLong())

        val os = exchange.responseBody
        os.write(response.toByteArray())
        os.close()
        os.flush()

        exchange.close()
    }
}
