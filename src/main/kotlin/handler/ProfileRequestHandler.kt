package com.gigadrivegroup.gameprofileserver.handler

import com.gigadrivegroup.gameprofileserver.MojangProfileManager
import com.gigadrivegroup.gameprofileserver.mojang.formatUUID
import com.gigadrivegroup.kotlincommons.feature.inject
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import java.util.UUID

public class ProfileRequestHandler(server: HttpServer) : Handler(server, "/profile") {
    private val profileManager: MojangProfileManager by inject()

    override fun handleExchange(exchange: HttpExchange) {
        val query = queryToMap(exchange.requestURI.rawQuery)
        if (!query.containsKey("query")) {
            writeResponse(exchange, "\"No query provided.\"", 400)
            return
        }

        val queryString = query["query"] ?: return

        val uuid: UUID = if (queryString.length != 32 && queryString.length != 36) {
            val fetchResult = profileManager.getUUIDFromUsername(queryString)

            if (fetchResult == null) {
                writeResponse(exchange, "\"No profile found.\"", 400)
                return
            }

            fetchResult
        } else {
            formatUUID(queryString)
        }

        val profile = profileManager.getProfile(uuid)
        if (profile != null) {
            profileManager.getUUIDFromUsername(profile.username) // make sure data is fetched

            profileManager.usernameUUIDCache
                .find {
                    it.data != null &&
                        it.uuid == profile.uuid &&
                        it.expiry > System.currentTimeMillis()
                }
                ?.let {
                    val data = it.data ?: return@let

                    profile.legacy = data.legacy ?: false
                    profile.demo = data.demo ?: false
                }
        }

        writeResponse(exchange, profile.toString(), 200)
    }
}
