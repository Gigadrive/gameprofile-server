package com.gigadrivegroup.gameprofileserver

import com.gigadrivegroup.gameprofileserver.mojang.MojangProfile
import com.gigadrivegroup.gameprofileserver.mojang.MojangProfileRaw
import com.gigadrivegroup.gameprofileserver.mojang.MojangUsernameUUIDResponseElement
import com.gigadrivegroup.gameprofileserver.mojang.formatUUID
import com.gigadrivegroup.kotlincommons.feature.CommonsManager
import com.gigadrivegroup.kotlincommons.util.GSON
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.gson.reflect.TypeToken
import java.util.UUID

public class MojangProfileManager : CommonsManager() {
    private val userAgent: String =
        "Minecraft GameProfile Server by Gigadrive UG (https://gitlab.com/Gigadrive/gameprofile-server)"
    private val cacheLength: Long = 60 * 1000

    public val profileCache: MutableSet<MojangProfileCacheEntry> = mutableSetOf()
    public val usernameUUIDCache: MutableSet<UsernameUUIDCacheEntry> = mutableSetOf()

    private var refreshCacheThreadActive: Boolean = true
    public var refreshCacheThread: Thread =
        object : Thread(null, null, "Refresh Cache Thread") {
            override fun run() {
                while (refreshCacheThreadActive) {
                    sleep(10 * 60 * 1000)

                    refreshCaches()
                    println("Refreshed caches.")
                }
            }
        }

    init {
        refreshCacheThread.start()
    }

    public fun refreshCaches() {
        profileCache.removeIf { it.expiry <= System.currentTimeMillis() }
        usernameUUIDCache.removeIf { it.expiry <= System.currentTimeMillis() }
    }

    public fun getProfile(uuid: UUID): MojangProfile? {
        var profile: MojangProfile? = null
        profileCache
            .find { it.expiry > System.currentTimeMillis() && it.profile?.uuid == uuid }
            ?.let { profile = it.profile }

        if (profile != null) {
            return profile
        }

        val rawProfile = fetchRawProfile(uuid)
        if (rawProfile == null) {
            println("Raw profile for UUID $uuid is null.")
            return null
        }

        profile = MojangProfile.fromRawProfile(rawProfile)
        if (profile == null) {
            println("Parsed profile for UUID $uuid is null.")
            return null
        }

        profileCache.add(MojangProfileCacheEntry(System.currentTimeMillis() + cacheLength, profile))

        return profile
    }

    public fun getUUIDFromUsername(username: String): UUID? {
        var uuid: UUID? = null
        usernameUUIDCache
            .find { it.expiry > System.currentTimeMillis() && it.data?.name.equals(username, true) }
            ?.let { uuid = it.uuid }

        if (uuid != null) {
            return uuid
        }

        val (_, _, result) =
            "https://api.mojang.com/profiles/minecraft"
                .httpPost()
                .body(GSON.toJson(arrayOf(username)))
                .appendHeader("Content-Type", "application/json")
                .appendHeader("User-Agent", userAgent)
                .responseString()

        when (result) {
            is Result.Failure -> {
                val exception = result.getException()
                println(
                    "Exception while fetching data for username \"${username}\" (${exception.javaClass.simpleName}): ${exception.message}")
            }
            is Result.Success -> {
                try {
                    val data =
                        GSON.fromJson<List<MojangUsernameUUIDResponseElement>>(
                                result.get(),
                                object : TypeToken<List<MojangUsernameUUIDResponseElement>>() {}
                                    .type)
                            .firstOrNull()
                            ?: throw Exception(
                                "Empty MojangUsernameUUIDResponseElement for username $username")

                    val uuid = formatUUID(data.id)

                    usernameUUIDCache.add(
                        UsernameUUIDCacheEntry(
                            System.currentTimeMillis() + cacheLength, data, uuid))

                    return uuid
                } catch (e: Exception) {
                    println(
                        "Exception while parsing data for username \"${username}\" (${e.javaClass.simpleName}): ${e.message}")
                }
            }
        }

        return null
    }

    private fun fetchRawProfile(uuid: UUID): MojangProfileRaw? {
        val (_, _, result) =
            "https://sessionserver.mojang.com/session/minecraft/profile/${uuid.toString().replace("-", "")}?unsigned=false"
                .httpGet()
                .appendHeader("User-Agent", userAgent)
                .responseString()

        when (result) {
            is Result.Failure -> {
                val exception = result.getException()
                println(
                    "Exception while fetching profile for UUID \"${uuid}\" (${exception.javaClass.simpleName}): ${exception.message}")
            }
            is Result.Success -> {
                try {
                    return GSON.fromJson(result.get(), MojangProfileRaw::class.java)
                } catch (e: Exception) {
                    println(
                        "Exception while parsing profile for UUID \"${uuid}\" (${e.javaClass.simpleName}): ${e.message}")
                }
            }
        }

        return null
    }

    override fun shutdown() {
        super.shutdown()

        profileCache.clear()
        usernameUUIDCache.clear()

        refreshCacheThreadActive = false
        refreshCacheThread.interrupt()
    }
}

public class MojangProfileCacheEntry(expiry: Long, public val profile: MojangProfile?) :
    CacheEntry(expiry)

public class UsernameUUIDCacheEntry(
    expiry: Long,
    public val data: MojangUsernameUUIDResponseElement?,
    public val uuid: UUID
) : CacheEntry(expiry)

public open class CacheEntry(public val expiry: Long)
