package com.gigadrivegroup.gameprofileserver.mojang

import com.gigadrivegroup.kotlincommons.util.GSON
import com.gigadrivegroup.kotlincommons.util.GSON_PRETTY
import com.google.gson.GsonBuilder
import java.util.Base64
import java.util.UUID

/** Profile entity that is returned later. */
public class MojangProfile(
    public val uuid: UUID,
    public val username: String,
    public val skin: MojangProfileSkinTexture?,
    public val cape: MojangProfileTexture?,
    public val signature: String?
) {
    public var legacy: Boolean? = null
    public var demo: Boolean? = null

    public companion object {
        public fun fromRawProfile(rawProfile: MojangProfileRaw): MojangProfile? {
            try {
                var skin: MojangProfileSkinTexture? = null
                var cape: MojangProfileTexture? = null
                var signature: String? = null

                val textureProperty = rawProfile.properties.firstOrNull()
                if (textureProperty != null) {
                    signature = textureProperty.signature

                    val textureValue =
                        GSON.fromJson(
                                String(Base64.getDecoder().decode(textureProperty.value)),
                                MojangProfileProperty::class.java)
                            .textures

                    if (textureValue != null) {
                        val skinTexture = textureValue.SKIN
                        if (skinTexture != null) {
                            skin =
                                MojangProfileSkinTexture(
                                    skinTexture.url,
                                    if (skinTexture.metadata != null)
                                        MojangProfileSkinTextureModel.ALEX
                                    else MojangProfileSkinTextureModel.STEVE)
                        }

                        val capeTexture = textureValue.CAPE
                        if (capeTexture != null) {
                            cape = MojangProfileTexture(capeTexture.url)
                        }
                    }
                }

                // https://stackoverflow.com/a/19399768/4117923
                return MojangProfile(
                    formatUUID(rawProfile.id),
                    rawProfile.name,
                    skin,
                    cape,
                    signature)
            } catch (e: Exception) {
                println("Error while decoding profile (${e.javaClass.simpleName}): ${e.message}")
                return null
            }
        }
    }

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(this)
    }
}

public open class MojangProfileTexture(public val url: String) {
    public val hash: String = url.split("/").last()
}

public class MojangProfileSkinTexture(
    url: String,
    public val model: MojangProfileSkinTextureModel
) : MojangProfileTexture(url)

public enum class MojangProfileSkinTextureModel {
    STEVE,
    ALEX
}

public class MojangProfileRaw(
    public val id: String,
    public val name: String,
    public val properties: Array<MojangProfileRawProperty>
)

public class MojangProfileRawProperty(
    public val name: String,
    public val value: String,
    public val signature: String?
)

public class MojangProfileProperty(
    public val timestamp: Long,
    public val profileId: String,
    public val profileName: String,
    public val signatureRequired: Boolean?,
    public val textures: MojangProfilePropertyTextures?
)

public class MojangProfilePropertyTextures(
    public val SKIN: MojangProfilePropertyTexture?,
    public val CAPE: MojangProfilePropertyTexture?
)

public class MojangProfilePropertyTexture(
    public val url: String,
    public val metadata: MojangProfilePropertyTextureMetadata?
)

public class MojangProfilePropertyTextureMetadata(public val model: String?)

public class MojangUsernameUUIDResponseElement(
    public val id: String,
    public val name: String,
    public val legacy: Boolean?,
    public val demo: Boolean?
)

public fun formatUUID(uuid: String): UUID =
    UUID.fromString(
        if (uuid.length == 32)
            uuid.substring(0, 8) +
                '-' +
                uuid.substring(8, 12) +
                '-' +
                uuid.substring(12, 16) +
                '-' +
                uuid.substring(16, 20) +
                '-' +
                uuid.substring(20)
        else uuid)