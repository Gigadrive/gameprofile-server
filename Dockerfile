FROM gradle:6.7-jdk15 as builder
COPY . .
RUN gradle build --no-daemon
FROM openjdk:15-jdk-alpine
COPY --from=builder /home/gradle/build/libs/gameprofile-server-1.0-SNAPSHOT-all.jar /gameprofile-server.jar
CMD ["java", "-jar", "/gameprofile-server.jar"]
